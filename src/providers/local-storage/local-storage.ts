import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { FixedLengthHashing } from '../../utils/hashing';
import { CryptoUtil } from '../../utils/crypto';
import { Item } from '../../models/item.model'

const LOGIN_KEY = 'login_key'
const STORAGE_KEY = 'mypasswords'
const ISLOGGEDIN = 'isLogged'
const COUNT_KEY = 'total'

@Injectable()
export class LocalStorageProvider {

  ALGORITHM = 'aes-256-ctr'
  private cnt: number

  constructor(private storage: Storage, private crypto: CryptoUtil) {
    storage.get(COUNT_KEY).then(res => {
      if (res) {
        this.cnt = res
      } else {
        this.cnt = 1
        this.storage.set(COUNT_KEY, this.cnt)
      }
    })
  }

  retrievePassword(password: string) {
    return this.storage.get(LOGIN_KEY).then(res => {
      if (res) {
        this.crypto.setPassword(res)
        return this.crypto.decrypt(password)
      } else {
        return false
      }
    })
  }

  setLoggedIn() {
    this.storage.set(ISLOGGEDIN, "true");
  }

  isLoggedIn () {
    this.storage.get(ISLOGGEDIN).then(res => {
      if (res && res === "true") {
        return true
      } else {
        return false
      }
    })
  }

  authenticate(password: string) {
    let hash = new FixedLengthHashing()
    return this.storage.get(LOGIN_KEY).then(res => {
      if (res && hash.verify(password, res)) {
        console.log("verified")
        return true
      } else {
        console.log('invalid user')
        return false
      }
    })
  }

  isRegistered () {
    return this.storage.get(LOGIN_KEY).then( res => {
      if (res)
        return true
      else
        return false
    })
  }

  savePassword (password: string) {
    let hash = new FixedLengthHashing()
    return this.storage.set(LOGIN_KEY, hash.digest(password))
  }

  changePassword (curr_pass: string, password: string) {
    this.storage.get(LOGIN_KEY).then(res => {
      if (res && res === new FixedLengthHashing().digest(curr_pass)) {
        return this.storage.set(LOGIN_KEY, new FixedLengthHashing().digest(password))
      } else {
        return false;
      }
    })
  }

  addItem (item: Item) {
    return this.get().then(res => {
      item.id = this.cnt
      this.storage.get(LOGIN_KEY).then(res => {
        if (res) {
          this.crypto.setPassword(res)
          item.password = this.crypto.encrypt(item.password)
        }
      }).then( _ => {
        if (res) {
          res.push(item)
          this.storage.set(STORAGE_KEY, res)
        } else {
          this.storage.set(STORAGE_KEY, [item])
        }
        this.cnt++
        this.storage.set(COUNT_KEY, this.cnt)
      })
    })
  }

  removeItem (id: number) {
    return this.get().then(res => {
      if (res) {
        res = res.filter(c => c.id !== id)
        this.storage.set(STORAGE_KEY, res)
      }
    })
  }

  updateItem (item: Item) {
    return this.get().then(res => {
      this.storage.get(LOGIN_KEY).then(res => {
        if (res) {
          this.crypto.setPassword(res)
          item.password = this.crypto.encrypt(item.password)
        }
      }).then( _ => {
        if (res) {
          let index = res.findIndex(c => c.id === item.id)
          res[index] = item
          this.storage.set(STORAGE_KEY, res)
        }
      })
    })
  }

  getItems () {
    return this.get().then(res => {
      return res;
    })
  }

  get() {
    return this.storage.get(STORAGE_KEY);
  }
}
