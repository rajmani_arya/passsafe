import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { IonicStorageModule } from "@ionic/storage";
import { Clipboard } from '@ionic-native/clipboard';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { LocalStorageProvider } from '../providers/local-storage/local-storage';
import { AddItemPage } from '../pages/add-item/add-item';
import { EditItemPage } from '../pages/edit-item/edit-item';
import { FixedLengthHashing } from '../utils/hashing';
import { CryptoUtil } from '../utils/crypto';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddItemPage,
    EditItemPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__secdb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddItemPage,
    EditItemPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Clipboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LocalStorageProvider,
    FixedLengthHashing,
    CryptoUtil
  ]
})
export class AppModule {}
