import { Component } from "@angular/core";
import { NavController, NavParams, ToastController } from "ionic-angular";
import { LocalStorageProvider } from "../../providers/local-storage/local-storage";

import {Item} from "../../models/item.model"

@Component({
  selector: "page-edit-item",
  templateUrl: "edit-item.html"
})
export class EditItemPage {

  item: Item

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private localStorage: LocalStorageProvider,
              private toastCtrl: ToastController) {
    this.item = navParams.get('item')
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad EditItemPage");
  }

  updateItem() {
    this.item.updatedon = new Date()
    this.localStorage.updateItem(this.item).then(res => {
      this.presentToast('Item was updated successfully')
    })
  }

  removeItem() {
    this.localStorage.removeItem(this.item.id).then(res => {
      this.presentToast('Item was deleted')
      this.navCtrl.pop()
    })
  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }
}
