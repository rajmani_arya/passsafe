import { Component } from '@angular/core';
import { ToastController, NavController, NavParams } from 'ionic-angular';

import { Item } from "../../models/item.model";
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

@Component({
  selector: 'page-add-item',
  templateUrl: 'add-item.html',
})
export class AddItemPage {

  item: Item

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public localStorage: LocalStorageProvider,
              private toastCtrl: ToastController) {
    this.resetItem()
  }

  resetItem() {
    this.item = {
      id: 0,
      name: '',
      url: '',
      username: '',
      password: '',
      updatedon: new Date()
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddItemPage')
  }

  saveItem() {
    console.log('Save ' + this.item.name)
    this.localStorage.addItem(this.item).then(res => {
      this.presentToast('Item was added successfully')
    })
  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }


}
