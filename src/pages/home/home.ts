import { Component } from '@angular/core';
import { NavController, AlertController, ToastController} from 'ionic-angular';
import {Item} from '../../models/item.model';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import { AddItemPage } from '../add-item/add-item';
import { EditItemPage } from '../edit-item/edit-item';
import { Clipboard } from '@ionic-native/clipboard';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  isSearching: boolean = false
  isLoggedIn: boolean = false
  isRegistered: boolean = false
  items: Item[]

  constructor(public navCtrl: NavController,
              private localStorage: LocalStorageProvider,
              private alertCtrl: AlertController,
              private toastCtrl: ToastController,
              private clipboard: Clipboard) {
  }

  sortByName (items: Item[]) {
    return items.sort((a,b) => {
      if (a.name > b.name) return 1
      else if (a.name === b.name) return 0
      else return -1
    })
  }

  ionViewWillEnter () {
    this.localStorage.isRegistered().then(res => {
      if (res)
        this.isRegistered = true;
      else
        this.isRegistered = false;
    })
    this.localStorage.getItems().then(res => {
      if (res) {
        this.items = this.sortByName(res)
      }
    })
  }

  loginModal() {
    let alert = this.alertCtrl.create({
      title: 'Login',
      inputs: [
        {
          name: 'password',
          placeholder: 'Password',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Login',
          handler: data => {
            this.localStorage.authenticate(data.password).then(res => {
              if (res && res === true) {
                this.localStorage.setLoggedIn()
                this.isLoggedIn = true
                this.presentToast("You are logged in.")
              } else {
                this.presentToast("Inavlid PIN")
                return false;
              }
            })
          }
        }
      ]
    });
    alert.present();
  }

  registerModal() {
    let alert = this.alertCtrl.create({
      title: 'Register',
      inputs: [
        {
          name: 'password',
          placeholder: 'Password',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Register',
          handler: data => {
            this.localStorage.savePassword(data.password).then(res => {
              if (res) {
                this.isRegistered = true
                this.presentToast("Successfully registered.")
              } else {
                this.presentToast("Error Occured!")
                return false;
              }
            })
          }
        }
      ]
    });
    alert.present();
  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  toggleSearchBar () {
    this.isSearching = !this.isSearching
  }

  editItem (event, item) {
    this.navCtrl.push(EditItemPage, {
      item: item
    })
  }

  addItem () {
    this.navCtrl.push(AddItemPage)
  }

  getItems(event) {
    let val = event.target.value
    if (this.isSearching && val != null && val.length !=0) {
      this.items = this.sortByName(this.items.filter(
        c => c.name.toLowerCase().startsWith(val.toLowerCase()))
      )
    } else {
      this.localStorage.getItems().then(res => {
        this.items = this.sortByName(res)
      })
    }
  }

  showPassword(event, item) {
    if (!this.isLoggedIn) {
      this.presentToast("Please Login")
    } else {
      this.localStorage.retrievePassword(item.password).then(res => {
        if (res) {
          this.clipboard.copy(res)
          let alert = this.alertCtrl.create({
            title: 'Name: ' + item.name,
            subTitle: 'Password: ' + res,
            buttons: ['Dismiss']
          });
          alert.setMessage('Password copied to clipboard!')
          alert.present();
        }
      })
    }
  }
}
