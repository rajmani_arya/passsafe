import * as Crypto from "crypto";

export class FixedLengthHashing {

  private hashAlgo: any;

  constructor() {
    this.hashAlgo = Crypto.createHash("sha256")
  }

  digest(text: string) {
    this.hashAlgo.update(text, "utf8")
    return this.hashAlgo.digest("hex")
  }

  verify(text: string, hash: string) {
    let hashPass = this.hashAlgo.update(text, "utf8").digest("hex")
    return hashPass === hash
  }
}
