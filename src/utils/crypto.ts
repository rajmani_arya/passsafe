import * as Crypto from "crypto";

export class CryptoUtil {
  private password: string
  ALGORITHM = 'aes-256-ctr'

  constructor() {
  }

  setPassword(password: string) {
    this.password = password
  }

  encrypt (text: string) {
    let cipher = Crypto.createCipher(this.ALGORITHM, this.password)
    let ciphertext = cipher.update(text, 'utf8', 'hex')
    ciphertext += cipher.final("hex")
    return ciphertext
  }

  decrypt (text: string) {
    let cipher = Crypto.createDecipher(this.ALGORITHM, this.password)
    let ciphertext = cipher.update(text, 'hex')
    return ciphertext.toString('utf-8')
  }
}
